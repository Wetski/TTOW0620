package com.example.veeti.launchamapwithimplicitintent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Console;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonMapClick(View view) {
        EditText editLatitude = (EditText) findViewById(R.id.editLatitude);
        EditText editLongitude = (EditText) findViewById(R.id.editLongitude);

        String strLatitude = editLatitude.getText().toString();
        String strLongitude = editLongitude.getText().toString();

        double latitude = Double.parseDouble(strLatitude);
        double longitude = Double.parseDouble(strLongitude);

        String text = latitude + ", " + longitude;

        Toast.makeText(getApplicationContext(), text,
                Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:"+latitude+","+longitude));
        startActivity(intent);
    }
}
