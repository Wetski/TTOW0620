package com.example.k1565.basicuicontrols_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle extras = getIntent().getExtras();

        String firstname = extras.getString("firstname");
        String lastname = extras.getString("lastname");

        TextView helloTextView = (TextView) findViewById(R.id.helloTextView);
        helloTextView.setText("Hello " + firstname + " " + lastname + "!");
    }
}
