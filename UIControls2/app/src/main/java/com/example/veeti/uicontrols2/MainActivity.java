package com.example.veeti.uicontrols2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.editLogin);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, new String[]
                {"Pasi","Veeti", "Adolf"});
        actv.setAdapter(aa);
    }

    public void loginButtonClicked(View view) {
        EditText login = (EditText) findViewById(R.id.editLogin);
        EditText password = (EditText) findViewById(R.id.editPassword);

        String text = login.getText().toString() + " " + password.getText().toString();

        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}
